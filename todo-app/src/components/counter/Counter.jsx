import React, { Component } from 'react';
import './counter.css';

import PropTypes from 'prop-types'

class Counter extends Component{
    constructor(){
        super(); // Error 1 !!! 
        this.state = {
            counter:0
        }
        //bindear con funciones de fuera
       this.increment = this.increment.bind(this);
       this.decrement = this.decrement.bind(this);
       this.reset = this.reset.bind(this);
    }
/*Una vez consuma incrementMethod manda a llamar a la función increment o decrement respectivamente, 
para sumar o restar by, el cuál es pasado por parámetros desde la clase hija CounterButton */
    render() {
        return (
          <div className="counter">
            {/*le paso por parámetro 1 y mando a incrementar con la prop incrementMethod a la clase CounterButton*/ }
            <CounterButton by={1} incrementMethod={this.increment} decrementMethod={this.decrement}/>
            <CounterButton by={5} incrementMethod={this.increment} decrementMethod={this.decrement}/>
            <CounterButton by={10} incrementMethod={this.increment} decrementMethod={this.decrement}/>
            <span className="count">{this.state.counter}</span>
            <div>
                <button className="reset" onClick={this.reset}>Reset</button>
            </div>
          </div>
        );
    }

    //método reset
    reset(){
        this.setState({counter: 0});
    }

    //actualizar el estado - counter++
    //Buena práctica es utilizar una arrow function + prevState por parámetro.
    increment(by) {
        this.setState(
            (prevState)=>{

            return {counter: prevState.counter + by}
        }
        );
    }
    //decrement
    decrement(by) {
        this.setState(
            (prevState)=>{

            return {counter: prevState.counter - by}
        }
        );
    }
}



//Se añadirá botones a la clase CounterButton la cual es un botón para cambiar el contador.
class CounterButton extends Component{

    constructor(){
        super(); // Error 1 !!! 
    }
    /*Como podemos ver, para no crear las funciones de antes, debemos enviarselo a través del onclick
    como arrow function y entre parámetros el by que enviamos antes, está lógica va al render de la clase 
    Counter */
    render(){
        return(
            <div className="counter">
                <button onClick={()=> this.props.incrementMethod(this.props.by)}>+{this.props.by}</button>
                <button onClick={()=> this.props.decrementMethod(this.props.by)}>-{this.props.by}</button>

            </div>
        )
    }

}
// si no se le envía un valor a by en el componente
CounterButton.defaultProps = {
    by : 1
}

//decirle a la prop que valor es, esto sirve para enviar en consola un error de que tipo de variable necesita.
CounterButton.propTypes = {
    by: PropTypes.number
}

export default Counter;
