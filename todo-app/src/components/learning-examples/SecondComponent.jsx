import React, { Component } from 'react';

//cuando tenemos dos clases a la segunda ponemos export y cuando la llamamos será entre llaves.


  //Class component
class SecondComponent extends Component {
    render() {
      return (
        <div className="FirstComponent">
           Second Component
        </div>
      );
    }
  }
  
  export default SecondComponent;