import React, { Component } from 'react';

//cuando tenemos dos clases a la segunda ponemos export y cuando la llamamos será entre llaves.

//Class component
class FirstComponent extends Component {
    render() {
      return (
        <div className="FirstComponent">
           First Coponent
        </div>
      );
    }
  }

  export default FirstComponent;