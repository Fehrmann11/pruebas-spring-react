import React, {Component} from 'react'
//import '../counter/counter.css'
import {BrowserRouter as Router,Route, Switch} from 'react-router-dom'

//importar componentes 
import AuthenticatedRoute from './AuthenticatedRoute'
import LoginComponent from './LoginComponent'
import ListTodosComponent from './ListTodosComponent'
import ErrorComponent from './ErrorComponent'
import WelcomeComponent from './WelcomeComponent'
import LogoutComponent from './LogoutComponent'
import FooterComponent from './FooterComponent'
import HeaderComponent from './HeaderComponent'
import TodoComponent from './TodoComponent'


/*Es el componente principal, el cuál con react router intercambia los componentes
como distintas pantallas.*/ 
class TodoApp extends Component{
    render(){
        return(
            <div className="TodoApp">
                {/*Así se crean las rutas con react router, y utilizamos el 
                componente AuthenticatedRouter,
                para ocultar rutas a ciertos usuarios. */}
                <Router>
                    
                    <HeaderComponent/>
                    <Switch>
                        <Route path="/" exact component={LoginComponent}/>
                        <Route path="/login" component={LoginComponent}/>
                        {/*AuthenticatedRouter es un componente el enviamos los props para saber si el
                        usuario está conectado en su sesión o no.
                        La jerarquía aquí es importante siempre los que tienen id deben ir arriba del
                        que no lo posean, por ejemplo todos */}
                        <AuthenticatedRoute path="/welcome/:name" component={WelcomeComponent}/>
                        <AuthenticatedRoute path="/todos/:id" component={TodoComponent}/>
                        <AuthenticatedRoute path="/todos" component={ListTodosComponent}/>
                        <AuthenticatedRoute path="/logout" component={LogoutComponent}/>
                        <Route component={ErrorComponent}/>
                    </Switch>
                    <FooterComponent/>
                   
                </Router>
                
            </div>
        )
    }
}




export default TodoApp;