import React , {Component} from 'react';

class FooterComponent extends Component{
    render(){
        return(
            <footer className="footer">
                <span className="text-muted">Todos los derechos reserados @Henry Fehrmann</span>
            </footer>
        )
    }
}

export default FooterComponent
