import { Form, Formik, Field, ErrorMessage } from 'formik'
import React, { Component } from 'react'
import moment from 'moment'
import TodoDataService from '../../api/todo/TodoDataService'
import AuthenticationService from './AuthenticationService'

class TodoComponent extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: this.props.match.params.id,
            description: '',
            targetDate: moment(new Date()).format('YYYY-MM-DD')
        }

        this.onSubmit = this.onSubmit.bind(this)
        this.validate = this.validate.bind(this)

    }

    //obtener los datos
    componentDidMount() {

        //88
        if (this.state.id === -1) {
            return
        }

        let username = AuthenticationService.getLoggedInUserName()
        TodoDataService.retrieveTodo(username, this.state.id)
            .then(response => this.setState({
                description: response.data.description,
                targetDate: moment(response.data.targetDate).format('YYYY-MM-DD')
            }))
    }

    /*87 botón save el que hace la actualización  de un 
    dato que se quiera actualizar y redirige a /todos*/
    onSubmit(values) {
        let username = AuthenticationService.getLoggedInUserName()
        //88
        let todo = {
            id: this.state.id,
            description: values.description,
            targetDate: values.targetDate
        }
        if (this.state.id === -1) {
            TodoDataService.createTodo(username, todo)
                .then(() => this.props.history.push('/todos'))
        } else {


            TodoDataService.updateTodo(username, this.state.id, todo)
                .then(() => this.props.history.push('/todos'))
        }

    }

    /*validación del formulario, tanto si no hay una descripción, tiene menos de tantos
    carácteres y también si la fecha es válida.
    */
    validate(values) {
        let errors = {}
        if (!values.description) {
            errors.description = 'Ingresa un descripción'
        } else if (values.description.length < 5) {
            errors.description = 'Debes ingresar al meno 5 carácteres en la descripción'
        }
        if (!moment(values.targetDate).isValid()) {
            errors.targetDate = 'Ingresar una fecha válida'
        }
        return errors
    }

    render() {
        //definición de variables
        let { description, targetDate } = this.state

        return (
            <div>
                <h1>Todo</h1>
                <div className="container">
                    {/*Los valores inciales, las funciones y que lo cambios
                    no se reflejen */}
                    <Formik

                        initialValues={{
                            description,
                            targetDate
                        }}
                        onSubmit={this.onSubmit}
                        validate={this.validate}
                        validateOnChange={false}
                        validateOnBlur={false}
                        /*reiniciar los parámetros */
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                /*descripción, fecha y botón */
                                <Form>
                                    <fieldset className="form-group">
                                        <label>Description</label>
                                        <Field className="form-control" type="text" name="description" />
                                        <ErrorMessage name="description" component="div"
                                            className="alert alert-danger" />
                                    </fieldset>
                                    <fieldset className="form-group">
                                        <label></label>
                                        <Field className="form-control" type="date" name="targetDate" />
                                        <ErrorMessage name="targetDate" component="div"
                                            className="alert alert-danger" />
                                    </fieldset>
                                    <button className="btn btn-primary" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>

            </div>
        )

    }
}

export default TodoComponent;