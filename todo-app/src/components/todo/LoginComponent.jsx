import AuthenticationService from './AuthenticationService';
import React , {Component} from 'react';


/*LoginComponente es el encargado de mostar el input de ingresar nombre y clave, además es el que 
tiene la lógica de enviar los token al local storage y confirmar el login */
class LoginComponent extends Component{

    constructor(props){
        super(props)
        this.state = {
            username:'ingrese su usuario',
            password:'ingrese su clave',
            hasLoginFailed: false,
            showSuccessMessege: false
        }
        this.handleChange = this.handleChange.bind(this)
        this.loginClicked = this.loginClicked.bind(this)
    }

   
    /*Hacemos una función capaz de consumir cualquier campo de texto, no importa
    si es de texto o password, hace la misma función, esto es gracias a [event.target.name]: event.target.value */
    handleChange(event){
        this.setState({
            [event.target.name] :event.target.value
        })
    }

    /*Función de clic botón, la cual pregunta si la clave está bien puesta
    el mensaje correcto es true y el incorrecto es false, si sucede lo contrario
    entonces los bool se niegan.*/
    loginClicked(){
        /*En esta sección mandamos a llamar a la clase AuthenticatoinService para que
        almacene el username y la password para la Session Storage una vez logeado en la app */
        if(this.state.username==='henry' && this.state.password==='123'){
            AuthenticationService.registerSuccessfulLogin(this.state.username,this.state.password);
            this.props.history.push(`/welcome/${this.state.username}`)
 
        }else{
            this.setState({
                showSuccessMessege:false,
                hasLoginFailed:true
            })
            console.log('Failed')
        }
          }

    render(){
        return(
            <div>
                <h1>Login</h1>
                <div className="container">
                    {/*Esto remplaza la función de validación antes creada para el login */}
                    {this.state.hasLoginFailed && <div className="alert alert-warning">Invalid Credentials</div>}
                    {this.state.showSuccessMessege && <div>Login successful</div>}
                    {/*Aquí creamos dos input y rellenamos los campos con this.state y como vamos a cambiar el contenido
                    tenemos que hacer un onchage={this.nombreFunción} */}
                    User Name: <input type="text" name="username" placeholder={this.state.username} onChange={this.handleChange}/>
                    Password: <input type="password" name="password" placeholder={this.state.password} onChange={this.handleChange}/>
                    <button className="btn btn-success" onClick={this.loginClicked}>Login</button>
                </div>
            </div>
        )
    }

    
}

export default LoginComponent