
import AuthenticationService from './AuthenticationService';
import React , {Component} from 'react';
import {Link} from 'react-router-dom'
import { withRouter } from 'react-router';

/*HeaderComponent es un menú el cuál pregunta a la clase AuthenticationService.isUserLoggedIn si la sesión
del usuario está activa, si lo está guarda la variable en isUserLoggedIn, luego en el navbar pregunta si
es true isUsserLoggedIn y le muestra el welcome, todos, login y logout, además para que el header sea dinámico
se necesita la liberría withRouter el cual puede hacer que cambie los estados del menú */
class HeaderComponent extends Component{
    render(){
        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
       
        return(
            <header>
                <nav className="navbar navbar-expand-md navbar-dark bg-dark">
                    <div><a href="/" className="navbar-brand">Henry!</a></div>
                    <ul className="navbar-nav">
                        {isUserLoggedIn && <li><Link  className="nav-link" to="/welcome/henry">Home</Link></li>}
                        {isUserLoggedIn &&<li ><Link className="nav-link" to="/todos">Todos</Link></li>}
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {!isUserLoggedIn &&<li ><Link className="nav-link" to="/login">Login</Link></li>}
                        {/*Aquí se cierra sesión en el local storage */}
                        {isUserLoggedIn &&<li ><Link className="nav-link" to="/logout" onClick={AuthenticationService.logout}>Logout</Link></li>}
                    </ul>
                </nav>
            </header>
         
        )
    }
}

export default withRouter(HeaderComponent);
