import React , {Component} from 'react';
//Este componente no se utiliza en la app, ya que al cerrar sesión no existe.
class LogoutComponent extends Component{
    render(){
        return(
            <div>
                <h1>Has terminado sesión</h1>
                <div className="container">
                    Gracias por usar la aplicación
                </div>
            </div>
        )
    }
}

export default LogoutComponent;