/*Esta clase es usada para poder almacenar el usuario y permitir estar en sesión */
class AuthenticationService{
    registerSuccessfulLogin(username,password){
        sessionStorage.setItem('authenticatedUser',username);
    }

    /*función para cerrar sesión (logout) */
    logout(){
        sessionStorage.removeItem('authenticatedUser');
    }

    /*Función para saber si está iniciada la sesión */
    isUserLoggedIn(){
        let user = sessionStorage.getItem('authenticatedUser')
        if(user===null) return false
        return true
    }
    /*Lo que hace este método es devolver un usuario 
    si la sesión está guardada */
    getLoggedInUserName(){
        let user = sessionStorage.getItem('authenticatedUser')
        if(user===null) return ''
        return user
    }
}

export default new AuthenticationService();