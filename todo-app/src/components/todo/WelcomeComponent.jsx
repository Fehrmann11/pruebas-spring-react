import React , {Component} from 'react';
import {Link} from 'react-router-dom'
import HelloWorldService from '../../api/todo/HelloWorldService'

/*WelcomeCompoente es el encargado de comsumir el nombre de url además de redirigir a el componente
TodoList */
class WelcomeComponent extends Component{
    constructor(props){
        super(props)
        this.retrieveWelcomeMessage = this.retrieveWelcomeMessage.bind(this)
        this.state ={
            welcomeMessage :''
        }
        this.handleSuccessfulResponse = this.handleSuccessfulResponse.bind(this)
        this.handleError = this.handleError.bind(this)
    }

    render(){
        //el .name viene del parámetro que nosotros definimos en Router welcome/:name
        return(
            <>
                <h1>Bienvenido!</h1>
                <div className="container">
                    Bienvenido {this.props.match.params.name}. Tu puedes gestionar tus todos <Link to="/todos">aquí</Link>.
                </div>
                <div className="container">
                    Clic aquí para personalizar tu mensaje de bienvenida.
                    <button className="btn btn-success" onClick={this.retrieveWelcomeMessage}>Obtener tu mensaje</button>
                </div>
                <div className="container">
                    {this.state.welcomeMessage}
                </div>
            </>
        ) 
    }
    //función que manda a llamar al servicio 
    retrieveWelcomeMessage(){
        //el parámetro viene por la URL.
                HelloWorldService.executeHelloWorldPathVariableService(this.props.match.params.name)
                .then(response => this.handleSuccessfulResponse(response))
                .catch(error => this.handleError(error))

    }

    handleSuccessfulResponse(response){
        this.setState({welcomeMessage: response.data.message})
    }

    handleError(error){
        console.log(error.response)
        //this.setState({welcomeMessage: response.data.message})
    }
}

export default WelcomeComponent