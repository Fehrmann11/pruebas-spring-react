
import React , {Component} from 'react';
import TodoDataService from '../../api/todo/TodoDataService'
import AuthenticationService from './AuthenticationService'
import moment from 'moment'

/*Este componente lo que realiza es hacer una lista de cosas, donde primero en el state define esas
cosas, luego en el render hace una tabla y se utiliza la función map para recorrer el array
luego se imprimen */
class ListTodosComponent extends Component{
    constructor(props){
        super(props)
        this.state = {
            todos : [],
            message: null
        }
        this.deleteTodoClick = this.deleteTodoClick.bind(this)
        this.refreshTodos = this.refreshTodos.bind(this)
        this.updateTodoClick = this.updateTodoClick.bind(this)
        this.addTodoClicked = this.addTodoClicked.bind(this)
    }

    /*Este método define una variable username el cual guarda el username con el método de la clase AUthenticationSErvice
    getLoggedInUserName */
    componentDidMount(){
        console.log('componentDidMount')
        this.refreshTodos();
       
    }

    //Actualizar los datos cuando se llama está función.
    refreshTodos(){
        let username = AuthenticationService.getLoggedInUserName()
        TodoDataService.retrieveAllTodos(username)
            .then(
                response => {
                    //console.log(response)
                    this.setState({
                        todos: response.data
                    })
                }
            )
    }


    //Función de eliminación 
    deleteTodoClick(id){
        let username = AuthenticationService.getLoggedInUserName()
        //console.log(id +" "+username);
        TodoDataService.deleteTodo(username,id)
        .then(
            response =>{
            /*Mostrará este mensaje al momento de que la eliminación sea 
                exitosa y recargará los datos */
                this.setState({message:`Delete of todo ${id} successful`})
                this.refreshTodos();
            }
        )
    }

    //88 Función para crear un nuevo Todo a la lista
    addTodoClicked(){
    
        this.props.history.push(`/todos/-1`)
    }

   //Función de actualización
    updateTodoClick(id){
    console.log('update' + id)
    this.props.history.push(`/todos/${id}`)
  
    }


    render(){
        return (
        <div>
            <h1>List Todos</h1>
            {/*si es false no se muestra el mensaje de exito al eliminar*/}
            {this.state.message && <div className="alert alert-success">{this.state.message}</div>}
            <div className="container">
                <table className="table">
                    <thead>
                        <tr>
                            <th>id</th>
                            <th>description</th>
                            <th>Target Date</th>
                            <th>Is Completed</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.todos.map(
                                todo => 
                                <tr key={todo.id}>
                                    <td>{todo.id}</td>
                                    <td>{todo.description}</td>
                                    <td>{moment(todo.targetDate).format('YYYY-MM-DD')}</td>
                                    <td>{todo.done.toString()}</td>
                                    <td><button className="btn btn-warning" 
                                    onClick={()=> this.updateTodoClick(todo.id)}>Update</button></td>
                                    <td><button className="btn btn-danger" 
                                    onClick={()=> this.deleteTodoClick(todo.id)}>Delete</button></td>
                                    
                                </tr>
                            )
                        
                        }
                    </tbody>
                </table>
                <div className="row">
                        <button className="btn btn-primary" onClick={this.addTodoClicked}>Add</button>
                </div>
            </div>
        </div>)
    }
}

export default ListTodosComponent;