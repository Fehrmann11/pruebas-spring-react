import axios from 'axios'
class HelloWorldService{
    executeHelloWorldService(){
        //retorna una promesa, la cuál hace una petición al backend.
        return axios.get('http://localhost:8080/hello-world');
    }

    executeHelloWorldBeanService(){
        //retorna una promesa, la cuál hace una petición al backend.
        return axios.get('http://localhost:8080/hello-world-bean');
    }

    executeHelloWorldPathVariableService(name){
        //retorna una promesa, la cuál hace una petición al backend.
        return axios.get(`http://localhost:8080/hello-world/path-variable/${name}`);
    }
}

export default new HelloWorldService()