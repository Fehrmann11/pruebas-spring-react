import axios from 'axios'

class TodoDataService{
    retrieveAllTodos(name){
        //retorna una promesa, la cuál hace una petición al backend.
        return axios.get(`http://localhost:8080/users/${name}/todos`);
    }

    //función para eliminar
    deleteTodo(name,id){
        return axios.delete(`http://localhost:8080/users/${name}/todos/${id}`);
    }

    //función para obtener todos los datos
    retrieveTodo(name,id){
        return axios.get(`http://localhost:8080/users/${name}/todos/${id}`);
    }

    //función para actualizar con el cuerpo del mensaje 87
    updateTodo(name,id,todo){
        return axios.put(`http://localhost:8080/users/${name}/todos/${id}`,todo);
    }

    //88 LLamada al servicio de creación 
    createTodo(name,todo){
        return axios.post(`http://localhost:8080/users/${name}/todos/`,todo); 
    }
}

export default new TodoDataService();