package com.henry.rest.webservices.restfulwebservices.todo;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

//La clase TodoHardedService se encarga de crear datos y retornanrlos
@Service
public class TodoHardcodedService {
    private static List<Todo> todos = new ArrayList();
    private static int idCounter=0;


    static{
        todos.add(new Todo(++idCounter,"henry","Aprendiendo react + java",new Date(),false));
        todos.add(new Todo(++idCounter,"Viviana","Aprendiendo maquillaje 2",new Date(),false));
        todos.add(new Todo(++idCounter,"Tutita","Aprendiendo goitear",new Date(),false));
    }

    public List<Todo> findAll(){
        return todos;
    }

    /*Delete empieza consumiendo la id, luego hace una consulta a encontrar los id
    * pregunta si la id no es null, si no la remueve */
    public Todo deleteById(long id){
        Todo todo = findById(id);

        if(todo==null) return null;

        if(todos.remove(todo)) {
            return todo;
        }
        return null;
    }

    public Todo save(Todo todo){
        //si no existe el id, crea uno
        if(todo.getId()==-1 || todo.getId()==0){
            todo.setId(++idCounter);
            todos.add(todo);
            //si existe, eliminalo y añade un todo
        }else{
            deleteById(todo.getId());
            todos.add(todo);
        }
        return todo;
    }

    //Se crea el método findById, se crea un for de todos los todo y ve si existe y retorna todo, sino null
    public Todo findById(long id) {
        for (Todo todo:todos){
            if(todo.getId()==id){
                return todo;
            }
        }
        return null;
    }
}
